<?php

class Elektronik extends Controller
{
	public function index()
	{
		$data = array(
			'judul' => "Daftar Elektronik",
			'elk' => $this->model('Elektronik_model')->getAllElektronik()
		);
		$this->view('templates/header', $data);
		$this->view('elektronik/index', $data);
		$this->view('templates/footer');
	}

//untuk menambah data // mengambil data dari form
	public function insert()
	{
		if(isset($_POST['submit'])){
			$data['gambar'] = $_POST["gambar"];
			$data['nama_barang'] = $_POST['nama_barang'];
			$data['merk'] = $_POST['merk'];
			$data['harga'] = $_POST['harga'];
			$data['deskripsi'] = $_POST['deskripsi'];
			$this->model('Elektronik_model')->insert("elektronik", $data);
			$this->index();
		}
	}

	public function ubah($id){
			$data = array(
					'judul' => 'update',
					'elk' => $this->model('Elektronik_model')->getElektronikById($id)
					);
					$this->view('templates/header', $data);
					$this->view('elektronik/ubah', $data);
					$this->view('templates/footer');
		}
//menghapuas Data
		public function delete($id)
		{
			$this->model('Elektronik_model')->delete("elektronik", $id);
			$this->index();
		}

//untuk update Data
public function update(){
		if(isset($_POST['ubah'])){
			$data['id'] = $_POST["id"];
			$data['nama_barang'] = $_POST["nama_barang"];
			$data['merk'] = $_POST['merk'];
			$data['harga'] = $_POST['harga'];
			$data['gambar']=$_POST['gambar'];
			$data['deskripsi']=$_POST['deskripsi'];
			$this->model('Elektronik_model')->update("elektronik", $data);
			$this->index();
		}
	}

	public function cari(){
		$search = $_POST["search"];
		if(isset($_POST["cari"]))
		$data = array(
				'judul' => 'Daftar Elektronik',
				'elk'=>$this->model('Elektronik_model')->cari("elektronikview", $search)
				);
		$this->view('templates/header', $data);
		$this->view('elektronik/index',$data);
		$this->view('templates/footer');
	}


}
