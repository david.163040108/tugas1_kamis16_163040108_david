<?php
//David Priyadi jangan lupa kunjungi dvstudio45.blogspot.com
class About extends Controller
{

	public function index($nama = "David Priyadi", $job = "Mahasiswa", $umur = "21")
	{
		$data = array(
				 'nama' => $nama,
				 'job' => $job,
				 'umur' => $umur,
				 'judul' => 'About Me'
				);
		$this->view('templates/header', $data);
		$this->view('about/index', $data);
		$this->view('templates/footer');
	}

	public function page()
	{
		$data['judul'] = "Pages";
		$this->view('templates/header', $data);
		$this->view('about/page');
		$this->view('templates/footer');
	}
}
