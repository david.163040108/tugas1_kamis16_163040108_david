
<div class="row">
    <div class="col">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?=BASEURL;?>/img/iklan1.jpg" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?=BASEURL;?>/img/iklan2.jpg" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?=BASEURL;?>/img/iklan3.jpg" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

    </div>
</div>

<div class="row mt-5 iklan mb-5">
  <div class="col-sm-3 ">
    <img src="<?=BASEURL;?>/img/iklan.jpg" alt="" style="width : 270px; height: 125px;">
  </div>
  <div class="col-sm-3">
    <img src="<?=BASEURL;?>/img/iklanb.jpg" alt="" style="width : 270px; height: 125px;">
  </div>
  <div class="col-sm-3">
    <img src="<?=BASEURL;?>/img/iklanc.jpg" alt="" style="width : 270px; height: 125px;">
  </div>
  <div class="col-sm-3">
    <img src="<?=BASEURL;?>/img/ikland.jpg" alt="" style="width : 270px; height: 125px;">
  </div>
</div>

<!-- ========================================================================================== -->
<div class="row">
  <div class="col mb-2 mt-3">
    <h3>Rekomendasi Untuk Anda</h3>
  </div>
</div>

<div class="row">
    <?php foreach ($data['bl'] as $key) : ?>
    <div class="animated bounceInUp slow col-2 mt-3">
        <div class="card" style="width: 175px; height: 350px;">
          <a href="<?= BASEURL; ?>/belanja/detail/<?= $key['id']; ?>" >
          <img class="card-img-top" src="<?=BASEURL;?>/img/<?=$key['gambar'];?>" alt="Card image cap">
          </a>
          <div class="card-body">
            <a href="<?= BASEURL; ?>/belanja/detail/<?= $key['id']; ?>" class="text-dark">
              <p class="card-title font-weight-bold">	<?= $key['nama_barang']; ?></p>
            </a>
            <p class="card-text text-secondary"><?= $key['merk']; ?></p>
            <a href="<?= BASEURL; ?>/belanja/detail/<?= $key['id']; ?>" class="text-danger">

                <p class="font-weight-bold font-weight-bold">Rp <?= $key['harga']; ?></p>
            </a>
          </div>
        </div>
      </div>

    <?php endforeach; ?>
  </div>
