<div class="row">
  <div class="col-sm-3">

  </div>
  <div class="col-sm-6">
    <h3 class="text-center">SEARCH UNTUK CRUD</h3>
    <form action="<?= BASEURL; ?>/Elektronik/cari" method="post">
      <div class="input-group mb-3">
        <input type="text" class="form-control"  name="search" id="judul"  aria-describedby="button-addon2">
      <div class="input-group-append">
        <button class="btn btn-primary" type="sumbit" name="cari" id="button-addon2">Cari</button>
      </div>
    </div>
    </form>
  </div>
  <div class="col-sm-3">
  </div>
</div>

  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambah1" data-whatever="@mdo">Tambah Data</button>

<!-- ============================================================================================================== -->
<!-- untuk menampilkan kedalam tabel -->
  <table class="table mt-4">
    <thead class="thead-dark">
      <tr class="text-center">
        <th scope="col">Gambar</th>
        <th scope="col">Nama</th>
        <th scope="col">Merk</th>
        <th scope="col">Harga</th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($data['elk'] as $key) : ?>
        <tr>
          <td>
            <img src="<?=BASEURL;?>/img/<?=$key['gambar'];?>" alt="" style="width:50px;">
          </td>
          <td><?= $key['nama_barang']; ?></td>
          <td><?= $key['merk']; ?></td>
          <td><?= $key['harga']; ?></td>
          <td><?= $key['deskripsi']; ?></td>
          <td>
            <a href="<?= BASEURL; ?>/Elektronik/ubah/<?= $key['id']; ?>" class="update" title="update">
              <img src="<?=BASEURL; ?>/img/si-glyph-document-plus.svg"/>
            </a>
            <p></p>
            <a href="<?= BASEURL; ?>/Elektronik/delete/<?= $key['id']; ?>" class="update" title="delete">
              <img src="<?=BASEURL; ?>/img/si-glyph-document-error.svg"/>
            </a>
          </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
<!-- ========================================================================================================== -->
<!-- form untuk menambah data -->
<div class="modal fade" id="tambah1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Elektronik</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">
              <form action="<?= BASEURL; ?>/Elektronik/insert" method="post">
                <div class="form-group">
                  <label for="nama"  class="col-form-label">Nama Elektronik</label>
                  <input type="text" name="nama_barang" id="nama_barang" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="merk" class="col-form-label">Merk</label>
                  <input type="text" name="merk" id="merk"  class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="harga" class="col-form-label">Harga</label>
                    <input type="text" name="harga" id="harga"  class="form-control" required>
                </div>
               <div class="form-group">
                    <label for="gambar" class="col-form-label">Gambar</label>
                    <input type="text" name="gambar" id="gambar"  class="form-control" placeholder="contoh : gambar1.jpg">
              </div>
              <div class="form-group">
                    <label for="gambar" class="col-form-label">Deskripsi</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" name="deskripsi" rows="3"></textarea>
              </div>
              <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
       </div>
    </div>
</div>
<!--=============================================================================================================================  -->
