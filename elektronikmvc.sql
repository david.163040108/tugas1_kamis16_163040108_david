-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2018 at 12:57 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elektronikmvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `elektronik`
--

CREATE TABLE `elektronik` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `deskripsi` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `elektronik`
--

INSERT INTO `elektronik` (`id`, `nama_barang`, `merk`, `harga`, `gambar`, `deskripsi`) VALUES
(13, 'MIYAKO Setrika', 'MIYAKO', '85.000', 'gambar4.jpg', 'Berpenampilan menarik tak akan pernah lepas dari identiknya kerapihan pakaian yang kita kenakan. Berpakaian rapi tidak kusut merupakan point penting untuk kita beraktifitas sehari-hari. Tapi kini hal itu semua dapat anda lakukan dengan mudah. Tak ada lagi pakaian yang kusut ketika Anda ingin bepergian karena dengan MIYAKO Setrika [EI-1000 M setrika listrik yang fungsional dengan harga yang ekonomis, Anda dapat dengan mudah menyetrika pakaian. '),
(14, 'MIYAKO Kipas Angin Lantai', 'MIYAKO', '244.000', 'gambar5.jpg', 'MIYAKO Kipas Angin Lantai [KAS 1618 B]\r\nNikmati udara sejuk dengan MIYAKO Kipas Angin Lantai [KAS 1618 B]. Anda tak perlu khawatir akan merasa kepanasan karena kipas angin ini membantu sirkulasi udara di ruangan Anda. Dengan bentuknya yang dapat didirikan, ditaruh di meja ataupun di dinding, Anda dapat menggunakan kipas angin ini sesuai dengan kebutuhan dan keinginan Anda.'),
(15, 'LG Mesin Cuci Twin Tub', 'LG', '2.585.000', 'gambar6.jpg', 'Tidak perlu repot mencuci pakaian menggunakan tangan yang memakan waktu lama dan dirasa kurang efisien. Sekarang sudah saatnya menggunakan cara baru yang lebih praktis, efisien, dan mudah dilakukan. Gunakan mesin cuci handal yang mampu menangani semua baju kotor anda dalam sekejap, pakaian bersih, tidak rusak dan bisa dikeringkan tanpa perlu repot memeras pakaian karena adanya pengering otomatis. '),
(18, 'SHARP 50 Inch TV LED', 'SHARP', '7.211.000', 'gambar1.jpg', 'Gambar Full HD Tajam dan Indah dengan X2 Master Engine\r\nTV Sharp Aquos LC50SA5200X didukung layar dengan format Full High Definition dengan resolusi 1920 x 1080 piksel. Resolusi yang lebih besar dari layar HD tersebut dapat menambah detail di gambar yang ditampilkan sehingga gambar lebih detail. Setiap gambar dapat ditampilkan dengan detail ketajaman yang menakjubkan. '),
(19, 'PHILIPS Blender', 'PHILIPS', '700.000', 'gambar2.jpg', 'Menghancurkan Es 20% lebih cepat dengan ProBlend 5\r\nMembuat smoothies kini lebih mudah dengan hadirnya Philips ProBlend 5. Dengan menggunakan blender yang satu ini, Anda dapat menghancurkan es 20% lebih cepat dibanding dengan blender pada umumnya. Pisau dengan teknologi ProBlend5 memungkinkan es dihancurkan dalam hitungan detik untuk menghasilkan smoothie beku sempurna. '),
(20, 'OPPO F9 4GB/64GB - Red', 'OPPO', '4.299.000', 'gambar3.jpg', 'Processor: Mediatek Helio P60 Octa-core (4x2.0 GHz Cortex-A73 & 4x2.0 GHz Cortex-A53)\r\nUkuran Layar: 6.3 inch\r\nKamera Belakang: 16MP+2MP\r\nKamera Depan: 25MP\r\nBaterai: 3500 mAh\r\nSistem Operasi: Android 8.0 Oreo');

-- --------------------------------------------------------

--
-- Stand-in structure for view `elektronikview`
-- (See below for the actual view)
--
CREATE TABLE `elektronikview` (
`id` int(11)
,`nama_barang` varchar(100)
,`merk` varchar(100)
,`harga` varchar(100)
,`gambar` varchar(100)
,`deskripsi` varchar(500)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `global`
-- (See below for the actual view)
--
CREATE TABLE `global` (
`id` int(11)
,`nama_barang` varchar(100)
,`harga` varchar(100)
,`merk` varchar(100)
,`gambar` varchar(100)
);

-- --------------------------------------------------------

--
-- Structure for view `elektronikview`
--
DROP TABLE IF EXISTS `elektronikview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `elektronikview`  AS  select `elektronik`.`id` AS `id`,`elektronik`.`nama_barang` AS `nama_barang`,`elektronik`.`merk` AS `merk`,`elektronik`.`harga` AS `harga`,`elektronik`.`gambar` AS `gambar`,`elektronik`.`deskripsi` AS `deskripsi` from `elektronik` ;

-- --------------------------------------------------------

--
-- Structure for view `global`
--
DROP TABLE IF EXISTS `global`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `global`  AS  select `elektronik`.`id` AS `id`,`elektronik`.`nama_barang` AS `nama_barang`,`elektronik`.`harga` AS `harga`,`elektronik`.`merk` AS `merk`,`elektronik`.`gambar` AS `gambar` from `elektronik` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `elektronik`
--
ALTER TABLE `elektronik`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `elektronik`
--
ALTER TABLE `elektronik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
